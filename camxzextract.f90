PROGRAM camxzextract

USE class_uam_iv

IMPLICIT NONE

!	------------------------------------------------------------------------------------------
!	Purpose:
!		Extracts selected z levels from an UAM-IV average file into another
!	Inputs:
!		Number of levels to be extracted
!		z level of each level to be extracted
! 		Input file name
! 		Output file name
!	Outputs:
!		UAM-IV average file with selected levels
!	By:
!		Pablo Garcia
!		05-2016
!	NOTE:
!		This code requires a Fortran 2003 compatible compiler

!	------------------------------------------------------------------------------------------
!	Declarations

!	Variables
	INTEGER :: out_nz								! Number of output levels
	INTEGER, ALLOCATABLE :: out_levels(:)			! Levels to be extracted

	CHARACTER(LEN=256) :: out_file					! Filename of the output file

!	Counters
	INTEGER :: i_nz

! 	Data type module
	TYPE(UAM_IV) :: fl								! Input average file
	TYPE(UAM_IV) :: fl_out							! Output average file with select z levels

!	------------------------------------------------------------------------------------------
!	Entry point
!	------------------------------------------------------------------------------------------
!
!	User IO
!
	WRITE(*,*) 'Extract selected z levels from an UAM-IV average file into another'

	WRITE(*,*) 'Number of levels to be extracted'
	READ (*,'(i)') out_nz
	WRITE(*,*) 'Extracting ', out_nz, ' levels'

! 	Allocate the Levels
	ALLOCATE(out_levels(out_nz))
	WRITE(*,*) 'z level of each level to be extracted'
	READ (*,*) (out_levels(i_nz),i_nz=1,out_nz)
	WRITE(*,*) 'Extracting this levels ', out_levels

! 	Get the input file name
	WRITE(*,*) 'Input file name: '
	READ (*,'(a)') fl%in_file

	WRITE(*,*) 'Output file name: '
	READ (*,'(a)') out_file

!	------------------------------------------------------------------------------------------
!	Level extraction
!	------------------------------------------------------------------------------------------
!
! 	Set the input file unit
	fl%unit = 21
! 	Change the default update_times to 1
! 	fl%update_times = 1
! 	Read the input file
	CALL read_uamfile(fl)

! 	Clone the input file
	fl_out = fl

! 	Set the output file unit
	fl_out%unit = 31
! 	Set the output file name
	fl_out%in_file = out_file
! 	Set the output file out_nz
	fl_out%nz = out_nz

!	------------------------------------------------------------------------------------------
! 	Split the code for different filetypes
	SELECT CASE (fl%ftype)
	CASE ('AIRQUALITY')
!	 	Allocate the output 3D concentration grid
		DEALLOCATE(fl_out%conc)
		ALLOCATE(fl_out%conc(fl_out%nx,fl_out%ny,fl_out%nz,fl_out%update_times,fl_out%nspec))

!		Exctract the z levels
! 		out_levels loop
		DO i_nz = 1,out_nz
			WRITE(*,*) 'Extracting level ', i_nz
			fl_out%conc(:,:,i_nz,:,:) = fl%conc(:,:,out_levels(i_nz),:,:)
		END DO

	CASE('BOUNDARY   ')
! 		Allocate the output boundary concentration array
		DEALLOCATE(fl_out%bound_conc)
		ALLOCATE(fl_out%bound_conc(MAXVAL(fl_out%ncell),fl_out%nz,fl_out%update_times,4,&
			&fl_out%nspec))

! 		Extract the z levels
! 		out_levels loop
		DO i_nz = 1,out_nz
			WRITE(*,*) 'Extracting level ', i_nz
			fl_out%bound_conc(:,i_nz,:,:,:) = fl%bound_conc(:,out_levels(i_nz),:,:,:)
		END DO
	END SELECT

!	------------------------------------------------------------------------------------------
!	Write the output file
!	------------------------------------------------------------------------------------------
!
! 	Write the output file
	CALL write_uamfile(fl_out)

END PROGRAM camxzextract
