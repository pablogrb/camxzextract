#!/bin/bash
rm class_uam_iv.mod
rm camxzextract
ifort class_uam_iv.f90 camxzextract.f90 -o camxzextract -g -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source
# ifort class_ptfile.f90 camxzextract.f90 -o camxzextract -O2 -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source

rm ic.camx-14z.bin bc.camx.2013193-14z.bin

./camxzextract << EOF
14
1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 14, 16, 18
ic.camx.bin
ic.camx-14z.bin
EOF

./camxzextract << EOF
14
1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 14, 16, 18
bc.camx.20130702.bin
bc.camx.14z.20130702.bin
EOF
